import urllib.request as req
class Robot:
    def __init__(self, url):
        self.url = url
        self.data = None
    def retrieve(self):
        if not self.data:
            opened = req.urlopen(self.url)
            self.data = opened.read().decode("utf-8")
            print("Descargando url: ", self.url)

    def show(self):
        self.retrieve()
        print(self.data)

    def content(self):
        self.retrieve()
        return self.data

class Cache:
    def __init__(self):
        self.cache = {}
    def retrieve(self, url):
        if url not in self.cache:
            robot = Robot(url)
            self.cache[url] = robot

    def content(self, url):
        self.retrieve(url)
        return self.cache[url].content()

    def show(self, url):
        print(self.content(url))

    def show_all(self):
        for url in self.cache:
            print(url)


if __name__ == "__main__":
    print("ROBOT")
    robot = Robot("https://labs.eif.urjc.es/")
    print(robot.url)
    robot.show()
    robot.retrieve()
    robot.content()

    print("CACHE")
    cache = Cache()
    cache.retrieve("https://chat.openai.com/")
    cache.show("https://labs.eif.urjc.es/")
    cache.retrieve("https://labs.eif.urjc.es/")
    cache.show_all()
